INSERT INTO `university_university` (`id`, `name`, `code`,`credits_number`) VALUES
(1, 'Universidad Nacional de Colombia', 'UN',50),
(2, 'Universidad Distrital Francisco José de Caldas', 'UD',21);

INSERT INTO `faculty_faculty` (`id`, `name`, `university_id`) VALUES
(1, 'Facultad de Ingeniería', 1),
(2, 'Facultad de Ingeniería', 2);

INSERT INTO `career_career` (`id`, `name`, `credits_number`, `faculty_id`) VALUES
(1, 'Ingeniería de Sistemas y Computación', 165, 1),
(2, 'Ingenieria Industrial', 168, 1),
(3, 'Ingeniería Mecánica', 180, 1),
(4, 'Ingeniería Mecatrónica', 179, 1),
(5, 'Ingeniería Electrónica', 172, 1),
(6, 'Ingeniería Eléctrica', 167, 1),
(7, 'Ingeniería Civil', 180, 1),
(8, 'Ingeniería Química', 180, 1),
(9, 'Ingeniería Agrícola', 180, 1),
(10, 'Ingeniería de Sistemas', 180, 2),
(11, 'Ingeniería Industrial', 167, 2),
(12, 'Ingeniería Electrónica', 163, 2),
(13, 'Ingeniería Eléctrica', 164, 2),
(14, 'Ingeniería Catastral y Geodesia ', 160, 2);

INSERT INTO `building_building` (`id`, `name`, `university_id`, `code`) VALUES
(1, 'Torre de Enfermería', 1, '101 '),
(2, 'Biblioteca Central', 1, '102'),
(3, 'Polideportivo - División de registro', 1, '103'),
(4, 'Auditorio León de Greiff', 1, '104'),
(5, ' Facultad de Derecho', 1, '201'),
(6, 'Edificio Orlando Fals Borda - Sociología ', 1, '205 '),
(7, 'Museo de Arquitectura Leopoldo Rother', 1, '207'),
(8, 'Facultad de Odontología', 1, '210'),
(9, 'Cafetería de Odontología', 1, '211'),
(10, 'Aulas de Ciencias Humanas', 1, '212'),
(11, 'Restaurante de Ciencias Humanas', 1, '213'),
(12, 'Edificio Antonio Nariño', 1, '214'),
(13, 'Edificio Francisco de Paula Santander', 1, '217'),
(14, 'Edificio Manuel Ancízar', 1, '224'),
(15, 'Edificio de Posgrados en Ciencias Humanas ', 1, '225'),
(16, 'Departamento de lenguas extranjeras', 1, '229'),
(17, 'Edificio de Enfermería', 1, '228'),
(18, 'Departamento de Lenguas', 1, '231'),
(19, 'Posgrados de Ciencias Económicas', 1, '238'),
(20, 'Edificio de Filosofía', 1, '239'),
(21, 'Capilla', 1, '251'),
(22, 'Escuela de Artes Plásticas', 1, '301'),
(23, 'Conservatorio de Música', 1, '305'),
(24, 'Talleres y Aulas de Construcción', 1, '309'),
(25, 'Facultad de Ciencias Económicas', 1, '310'),
(26, 'Edificio SINDU - Postgrados en Arquitectura', 1, '314'),
(27, 'Facultad de Ingeniería', 1, '401'),
(28, 'Edificio Yu Takeuchi', 1, '404'),
(29, 'Postgrados en Matemáticas y Física', 1, '405'),
(30, 'Instituto de Extensión e Investigación', 1, '406'),
(31, 'Postgrados en Materiales y Procesos de Manufactura', 1, '407'),
(32, 'CADE de Ingeniería', 1, '408'),
(33, 'Laboratorio de Hidráulica', 1, '409'),
(34, 'Laboratorios de Ingeniería', 1, '411'),
(35, 'Laboratorio de Ingeniería Química', 1, '412'),
(36, 'Observatorio Astronómico', 1, '413'),
(37, 'Departamento de Biología', 1, '421'),
(38, 'Instituto de Ciencias Naturales', 1, '425'),
(39, 'Instituto de Genética', 1, '426'),
(40, 'Departamento de Farmacia', 1, '450'),
(41, 'Departamento de Química', 1, '451'),
(42, 'Postgrados en Bioquímica y Carbones', 1, '452'),
(43, 'Aulas de Ingeniería', 1, '453'),
(44, 'Edificio de Ciencia y Tecnología CyT', 1, '454'),
(45, 'Facultad de Medicina', 1, '471'),
(46, 'Facultad de Ciencias', 1, '476'),
(47, 'Aulas de Informática', 1, '477'),
(48, 'Facultad de Medicina Veterinaria y Zootecnia', 1, '481'),
(49, 'Departamento de Agronomía', 1, '500');

INSERT INTO `typology_typology` (`id`, `name`) VALUES
(1, 'Disciplinar Obligatoria'),
(2, 'Fundamentación Obligatoria'),
(3, 'Disciplinar Optativa'),
(4, 'Fundamentación Optativa'),
(5, 'Nivelación'),
(6, 'Libre Elección');

INSERT INTO `typology_university_typologybyuniversity` (`id`, `typology_id`, `university_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1);

INSERT INTO `teacher_teacher` (`id`, `name`, `status`) VALUES
(1, 'Alejandra Sanchez Vasquez', 0),
(2, 'Carolina Neira Jimenez', 0),
(3, 'Daniel Alfonso Bojaca Torres', 0),
(4, 'Diana Marcela Serrano Rodriguez', 0),
(5, 'Edwin Aldemar Jiménez Quiroga', 0),
(6, 'Herbert Alonso Dueñas Ruiz', 0),
(7, 'Ibeth Marcela Rubio Perilla', 0),
(8, 'Jeanneth Galeano Peñaloza', 0),
(9, 'John Jaime Rodriguez Vega', 0),
(10, 'Jose Reinaldo Montanez Puentes', 0),
(11, 'Leonardo Rendon Arbelaez', 0),
(12, 'Lorenzo Maria Acosta Gempeler', 0),
(13, 'Margaret Johanna Garzon Merchan', 0),
(14, 'Margarita Ospina Pulido', 0),
(15, 'Martha Cecilia Moreno Penagos', 0),
(16, 'Milton Armando Reyes Villamil', 0),
(17, 'Natalia Camila Pinzon Cortes', 0),
(18, 'Sherly Paola Alfonso', 0),
(19, 'Andres Chacon Capera', 0),
(20, 'Arcenio Pecha Castiblanco', 0),
(21, 'Camilo Andrés Rodríguez Rodríguez', 0),
(22, 'Christian Nolasco Serna', 0),
(23, 'Diego Arturo Niño Torres', 0),
(24, 'Edixon Manuel Rojas Santana', 0),
(25, 'Eduardo Cardenas Gomez', 0),
(26, 'Efrain Camilo Pardo Garcia', 0),
(27, 'Fabian Ricardo Muñoz Reyes', 0),
(28, 'Fabio Calderón Mateus', 0),
(29, 'Fausto Camilo Bolivar Barbosa', 0),
(30, 'Francisco Albeiro Gomez Jaramillo', 0),
(31, 'Franqui Solis Cardenas Poloche', 0),
(32, 'Gabriel Ignacio Padilla Leon', 0),
(33, 'German Preciado Lopez', 0),
(34, 'German Eduardo Fonseca Buitrago', 0),
(35, 'Gustavo Adolfo Nieto Clavijo', 0),
(36, 'Hernan Garzon Gutierrez', 0),
(37, 'Ivan Castro Chadid', 0),
(38, 'Ivon Andrea Dorado Correa', 0),
(39, 'Jaime Andres Robayo Mesa', 0),
(40, 'Jaime Andrés Gómez Ortiz', 0),
(41, 'Jenny Alejandra Pabon Cadavid', 0),
(42, 'Jose Israel Merchan Herrera', 0),
(43, 'José Manuel Chauta Torres', 0),
(44, 'Juan Andres Montoya Arguello', 0),
(45, 'Juan Carlos Galvis Arrieta', 0),
(46, 'Juan Carlos Mendivelso Moreno', 0),
(47, 'Juan Daniel Lopez', 0),
(48, 'Luis Roberto Quicazan Acosta', 0),
(49, 'Manuel Jair Medina Luna', 0),
(50, 'Martha Mancera', 0),
(51, 'Mauricio Lopez Hernandez', 0),
(52, 'Mauricio Bogoya Lopez', 0),
(53, 'Nicolas Martinez Alba', 0),
(54, 'Olga Lucia Escobar Medina', 0),
(55, 'Omar Duque Gomez', 0),
(56, 'Oscar Alberto Rodriguez Melendez', 0),
(57, 'Oscar Mauricio Parra Baquero', 0),
(58, 'Paul Harritson Villamil Hernandez', 0),
(59, 'Rafael Melo Jimenez', 0),
(60, 'Rene Erlin Castillo', 0),
(61, 'Sandra Carolina Garcia Martinez', 0),
(62, 'Sebastian Higuera Rincon', 0),
(63, 'Serafin Bautista Diaz', 0),
(64, 'Stiven Leonardo Silva Castillo', 0),
(65, 'German Jairo Hernandez Perez', 0),
(66, 'Iván Mauricio Rueda Cáceres', 0),
(67, 'Jenny Marcela Sanchez Torres', 0),
(68, 'Jose Ismael Peña Reyes', 0),
(69, 'Libia Denise Cangrejo Aljure', 0),
(70, 'Luis Fernando Niño Vasquez', 0),
(71, 'Alexei Gabriel Ochoa Duarte', 0),
(72, 'Andrés Ricardo Arévalo Murillo', 0),
(73, 'Edwin Andres Niño Velasquez', 0),
(74, 'Edwin Camilo Cubides Garzon', 0),
(75, 'Fabio Augusto Gonzalez Osorio', 0),
(76, 'Fernando Salazar Delgado', 0),
(77, 'Gabriel Jose Mañana Guichon', 0),
(78, 'Jhon Alexander López Fajardo', 0),
(79, 'Jonatan Gomez Perdomo', 0),
(80, 'Juan Carlos Torres Pardo', 0),
(81, 'Oscar Agudelo Rojas', 0),
(82, 'Oswaldo Rojas Camacho', 0),
(83, 'Sandra Liliana Rojas Martinez', 0),
(84, 'Santiago Carvajal', 0),
(85, 'Stephanie Torres Jimenez', 0),
(86, 'Tito Florez Calderon', 0),
(87, 'No Informado', 0),
(88, 'Kevin Julian Franco Cuervo', 0),
(89, 'Felipe Restrepo Calle', 0),
(90, 'Alvaro Dario Rodriguez Caicedo', 0),
(91, 'Brayan Stiven Cabra Ramirez', 0),
(92, 'Alexandre Sinitsyne', 0),
(93, 'Yiby Karolina Morales Pinto', 0),
(94, 'Myriam Leonor Campos Florez', 0),
(95, 'Andres Alejandro Rubiano Suarez', 0),
(96, 'Carlos Andres Giraldo Hernandez', 0),
(97, 'Gustavo Eduardo Arengas Reines', 0),
(98, 'John Alexander Cruz Morales', 0),
(99, 'Javier Mauricio Sierra', 0),
(100, 'Jose Exequiel Fuentes Gil', 0);

INSERT INTO `classroom_classroom` (`id`, `name`, `building_id`) VALUES
(1, '101', 27);

INSERT INTO `subject_subject` (`id`, `name`, `credits_number`, `typology_id`) VALUES
(1, 'Matemáticas Básicas', 4, 5),
(2, 'Lecto-Escritura', 4, 5),
(3, 'Inglés I', 3, 5),
(4, 'Inglés II', 3, 5),
(5, 'Inglés III', 3, 5),
(6, 'Inglés IV', 3, 5),
(7, 'Calculo Diferencial', 4, 4),
(8, 'Introdución a la Ingeniería de Sistemas y Computación', 3, 1),
(9, 'Programación de Computadores', 3, 3),
(10, 'Calculo Integral', 4, 4),
(11, 'Álgebra Lineal', 4, 4),
(12, 'Fundamentos de Mecánica', 4, 4),
(13, 'Programación Orientada a Objetos', 3, 3),
(14, 'Calculo en Varias Variables', 4, 4),
(15, 'Probabilidad y Estadística', 3, 4),
(16, 'Matemáticas Discretas I', 4, 4),
(17, 'Bases de Datos', 3, 3),
(18, 'Elementos de Computadores', 3, 3),
(19, 'Fundamentos de Electricidad y Magnetismo', 4, 4),
(20, 'Ingeniería Económica ', 3, 4),
(21, 'Matemáticas Discretas II', 4, 4),
(22, 'Estructuras de Datos', 3, 3),
(23, 'Arquitectura de Computadores', 3, 3),
(24, 'Modelos y Simulación', 3, 3),
(25, 'Gerencia y Gestión de Proyectos', 3, 4),
(26, 'Redes de Computadores', 3, 3),
(27, 'Ingeniería de Software I', 3, 3),
(28, 'Introducción a la Teoría de la Computación', 4, 4),
(29, 'Optimización', 3, 3),
(30, 'Métodos Numéricos', 3, 4),
(31, 'Sistemas de Información', 3, 3),
(32, 'Ingeniería de Software II', 3, 3),
(33, 'Algoritmos', 3, 3),
(34, 'Sistemas Operativos', 3, 3),
(35, 'Modelos estocásticos y simulación en computación y comunicaciones', 3, 3),
(36, 'Teoría de la Información y Sistemas de Comunicaciones', 3, 3),
(37, 'Pensamiento sistemico', 3, 3),
(38, 'Arquitectura de Software', 3, 3),
(39, 'Lenguajes de Programación', 3, 3),
(40, 'Introducción a los Sistemas Inteligentes', 3, 3),
(41, 'Computación paralela y distribuida', 3, 3),
(42, 'Computación visual', 3, 3),
(43, 'Taller de proyectos interdisciplinarios', 3, 3),
(44, 'Arquitectura de infraestructura y gobierno de TICs', 3, 3),
(45, 'Criptografía y seguridad de la información', 3, 3);

INSERT INTO `subject_career_subjectbycareer` (`id`, `career_id`, `subject_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(17, 1, 17),
(18, 1, 18),
(19, 1, 19),
(20, 1, 20),
(21, 1, 21),
(22, 1, 22),
(23, 1, 23),
(24, 1, 24),
(25, 1, 25),
(26, 1, 26),
(27, 1, 27),
(28, 1, 28),
(29, 1, 29),
(30, 1, 30),
(31, 1, 31),
(32, 1, 32),
(33, 1, 33),
(34, 1, 34),
(35, 1, 35),
(36, 1, 36),
(37, 1, 37),
(38, 1, 38),
(39, 1, 39),
(40, 1, 40),
(41, 1, 41),
(42, 1, 42),
(43, 1, 43),
(44, 1, 44),
(45, 1, 45);

INSERT INTO `group_group` (`id`, `classroom_id`, `subject_id`, `teacher_id`, `number`) VALUES
(1, 1, 1, 12, '5'),
(2, 1, 1, 87, '13'),
(3, 1, 1, 87, '14'),
(4, 1, 8, 70, '1'),
(5, 1, 8, 68, '2'),
(6, 1, 9, 88, '11'),
(7, 1, 9, 73, '15'),
(8, 1, 9, 89, '18'),
(9, 1, 9, 90, '8'),
(10, 1, 9, 85, '9'),
(11, 1, 7, 91, '1'),
(12, 1, 7, 92, '10'),
(13, 1, 7, 93, '11'),
(14, 1, 7, 92, '12'),
(15, 1, 7, 57, '13'),
(16, 1, 7, 62, '14'),
(17, 1, 7, 35, '15'),
(18, 1, 7, 94, '16'),
(19, 1, 7, 62, '17'),
(20, 1, 7, 39, '18'),
(21, 1, 7, 95, '19'),
(22, 1, 7, 37, '2'),
(23, 1, 7, 26, '21'),
(24, 1, 7, 96, '22'),
(25, 1, 7, 97, '23'),
(26, 1, 7, 98, '24'),
(27, 1, 7, 93, '25'),
(28, 1, 7, 91, '26'),
(29, 1, 7, 99, '27'),
(30, 1, 7, 33, '28'),
(31, 1, 7, 48, '3'),
(32, 1, 7, 52, '30'),
(33, 1, 7, 100, '31'),
(34, 1, 7, 42, '32'),
(35, 1, 7, 41, '33'),
(36, 1, 7, 32, '6'),
(37, 1, 7, 26, '7'),
(38, 1, 7, 96, '8'),
(39, 1, 7, 57, '9');

INSERT INTO `classes_class` (`id`, `day`, `time_start`, `time_end`, `group_id`) VALUES
(1, 'Miercoles', '7', '9', 1),
(2, 'Viernes', '7', '9', 1),
(3, 'Lunes', '7', '9', 2),
(4, 'Lunes', '11', '13', 3),
(5, 'Lunes', '9', '11', 4),
(6, 'Miercoles', '9', '11', 4),
(7, 'Lunes', '9', '11', 5),
(8, 'Martes', '9', '11', 5),
(9, 'Martes', '11', '13', 6),
(10, 'Jueves', '11', '13', 6),
(11, 'Lunes', '11', '13', 7),
(12, 'Miercoles', '11', '13', 7),
(13, 'Lunes', '7', '9', 8),
(14, 'Miercoles', '7', '9', 8),
(15, 'Martes', '14', '16', 9),
(16, 'Jueves', '14', '16', 9),
(17, 'Martes', '11', '13', 10),
(18, 'Jueves', '11', '13', 10),
(19, 'Lunes', '7', '9', 11),
(20, 'Miercoles', '7', '9', 11),
(21, 'Lunes', '16', '18', 12),
(22, 'Miercoles', '16', '18', 12),
(23, 'Lunes', '16', '18', 13),
(24, 'Miercoles', '16', '18', 13),
(25, 'Lunes', '18', '20', 14),
(26, 'Miercoles', '18', '20', 14),
(27, 'Lunes', '18', '20', 15),
(28, 'Miercoles', '18', '20', 15),
(29, 'Martes', '7', '9', 16),
(30, 'Jueves', '7', '9', 16),
(31, 'Martes', '7', '9', 17),
(32, 'Jueves', '7', '9', 17),
(33, 'Martes', '9', '11', 18),
(34, 'Jueves', '9', '11', 18),
(35, 'Martes', '11', '13', 19),
(36, 'Jueves', '11', '13', 19),
(37, 'Martes', '11', '13', 20),
(38, 'Jueves', '11', '13', 20),
(39, 'Martes', '14', '16', 21),
(40, 'Jueves', '14', '16', 21),
(41, 'Lunes', '7', '9', 22),
(42, 'Miercoles', '7', '9', 22),
(43, 'Martes', '14', '16', 23),
(44, 'Jueves', '14', '16', 23),
(45, 'Martes', '14', '16', 24),
(46, 'Jueves', '14', '16', 24),
(47, 'Martes', '16', '18', 25),
(48, 'Jueves', '16', '18', 25),
(49, 'Martes', '16', '18', 26),
(50, 'Jueves', '16', '18', 26),
(51, 'Martes', '16', '18', 27),
(52, 'Jueves', '16', '18', 27),
(53, 'Martes', '18', '20', 28),
(54, 'Jueves', '18', '20', 28),
(55, 'Martes', '18', '20', 29),
(56, 'Jueves', '18', '20', 29),
(57, 'Miercoles', '9', '11', 30),
(58, 'Viernes', '9', '11', 30),
(59, 'Lunes', '7', '9', 31),
(60, 'Miercoles', '7', '9', 31),
(61, 'Martes', '7', '9', 32),
(62, 'Jueves', '7', '9', 32),
(63, 'Lunes', '7', '9', 33),
(64, 'Miercoles', '7', '9', 33),
(65, 'Lunes', '18', '20', 34),
(66, 'Miercoles', '18', '20', 34),
(67, 'Martes', '18', '20', 35),
(68, 'Jueves', '18', '20', 35),
(69, 'Lunes', '9', '11', 36),
(70, 'Miercoles', '9', '11', 36),
(71, 'Lunes', '11', '13', 37),
(72, 'Miercoles', '11', '13', 37),
(73, 'Lunes', '11', '13', 38),
(74, 'Miercoles', '11', '13', 38),
(75, 'Lunes', '14', '16', 39),
(76, 'Miercoles', '14', '16', 39);


CREATE VIEW vw_subjects_by_career AS
SELECT
  DISTINCT
    S.id AS id
  , C.id AS career_id
  , F.name AS faculty_name
  , C.name AS career_name
  , S.name AS name
  , T.name AS typology_name
FROM
  subject_career_subjectbycareer AS SC
  JOIN subject_subject AS S ON S.id = SC.subject_id
  JOIN career_career AS C ON C.id = SC.career_id
  JOIN faculty_faculty AS F ON F.id = C.faculty_id
  JOIN typology_typology AS T ON T.id = S.typology_id


CREATE VIEW vw_subjects_by_classes_by_group AS
SELECT
  DISTINCT
    G.id AS id
  , S.id AS subject_id
  , SC.id AS classes_id
  , G.number AS name
  , TY.name AS typology_name
  , SC.day AS day
  , SC.time_start AS time_start
  , SC.time_end AS time_end
  , T.name AS teacher_name
  , S.name AS subject_name
  , S.credits_number AS credits_number
  , CR.name AS classroom_name
  , B.name AS building_name
FROM
    classes_class AS SC
    JOIN group_group AS G ON G.id = SC.group_id
    JOIN subject_subject AS S ON S.id = G.subject_id
    JOIN teacher_teacher AS T ON T.id = G.teacher_id
    JOIN classroom_classroom AS CR ON CR.id = G.classroom_id
    JOIN building_building AS B ON B.id = CR.building_id
    JOIN subject_career_subjectbycareer AS SCC ON SCC.subject_id = S.id
    JOIN career_career AS C ON C.id = SCC.career_id
    JOIN faculty_faculty AS F ON F.id = C.faculty_id
    JOIN typology_typology AS TY ON TY.id = S.typology_id
    JOIN university_university AS U ON U.id = F.university_id



CREATE VIEW vw_classes_and_groups_by_student AS
SELECT
  DISTINCT
    S.user_id AS id
  , SJ.id AS subject_id
  , G.id AS group_id
  , C.id AS classes_id
  , GS.id AS group_student
  , G.number AS group_number
  , C.day AS day
  , C.time_start AS time_start
  , C.time_end AS time_end
  , T.name AS teacher_name
  , SJ.name AS subject_name
  , SJ.credits_number AS credits_number
  , CS.name AS classroom_name
  , B.name AS building_name
FROM
  classes_class AS C
  JOIN group_group AS G ON G.id = C.group_id
  JOIN group_student_groupbystudent AS GS ON GS.group_id = G.id
  JOIN student_student AS S ON S.user_id = GS.student_id
  JOIN teacher_teacher AS T ON T.id = G.teacher_id
  JOIN subject_subject AS SJ ON SJ.id = G.subject_id
  JOIN classroom_classroom AS CS ON CS.id = G.classroom_id
  JOIN building_building AS B ON B.id = CS.building_id


CREATE VIEW vw_averages_by_teacher AS
SELECT
  DISTINCT
    T.id as id
  , T.name as name
  , ROUND(AVG(R.amenity_qualification),1) AS amenity_average
  , ROUND(AVG(R.classes_qualification),1) AS classes_average
  , ROUND(AVG(R.experience_qualification),1) AS experience_average
  , ROUND(AVG(R.explication_qualification),1) AS explication_average
  , ROUND(AVG(R.student_qualification),1) AS student_average
  , ROUND(AVG(R.final_note),1) AS note_average
FROM
  reference_reference AS R
  JOIN teacher_teacher as T on T.id = R.teacher_id
GROUP BY id


CREATE VIEW vw_references_by_teacher AS
SELECT
  DISTINCT
    t.id as id
  , S.name as name
  , r.amenity_qualification AS amenity_qualification
  , r.classes_qualification AS classes_qualification
  , r.experience_qualification AS experience_qualification
  , r.explication_qualification AS explication_qualification
  , r.student_qualification AS student_qualification
  , r.final_note AS final_note
  , r.description as description
  , r.year as year
  , r.semester as semester
FROM
  reference_reference AS R
  JOIN teacher_teacher as T on T.id = R.teacher_id
  JOIN subject_subject as S on S.id = R.subject_id



CREATE VIEW vw_subjects_by_teacher AS
SELECT
  DISTINCT
    T.id as id
  , S.id as subject_id
  , S.name as name
  FROM
    group_group AS G
    JOIN teacher_teacher as T on T.id = G.teacher_id
    JOIN subject_subject as S on S.id = G.subject_id


CREATE VIEW vw_history_by_student AS
  SELECT
    DISTINCT
      S.user_id as id
    , S.credits_semester as credits_semester
    , ROUND(H.average,1) AS average
    , C.credits_number as credits_number
    , H.completed_credits AS completed_credits
    , ROUND((H.completed_credits*100/C.credits_number),1) AS progress
  FROM
    history_history AS H
    JOIN student_student as S on S.user_id = H.student_id
    JOIN career_career as C on C.id = S.career_id


CREATE VIEW vw_subjects_by_student AS
  SELECT
    DISTINCT
      S.user_id as id
    , SJ.name as subject
    , G.id as 'group'
    , SJ.credits_number as credits_number
    , ROUND(SUM((SN.value*(SN.percent/100))),1) AS note_acumulated
    , ROUND(SUM(((SN.value*(SN.percent/100)))*5) / (SUM((5*(SN.percent/100)))),1)  AS positive_note
    , SUM(SN.percent) as percent_acumulated
    FROM
      subject_student_note_notestudentbysubject AS SN
      JOIN group_group as G on G.id = SN.group_id
      JOIN student_student AS S on S.user_id = SN.student_id
      JOIN subject_subject as SJ on SJ.id = G.subject_id
      GROUP BY id,subject
