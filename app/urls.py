from django.contrib import admin
from django.conf import settings
from django.conf.urls import url, include
from rest_framework.authtoken import views
from rest_framework_swagger.views import get_swagger_view
from rest_framework.schemas.coreapi import AutoSchema
from rest_framework.documentation import include_docs_urls
from rest_framework import routers
from django.conf.urls.static import static
from models.student.serializers import CustomAuthToken, CustomAuthLogout
from models.university.views import home

schema_view = get_swagger_view(title='U4u API')
router = routers.DefaultRouter()

#Admin
urlpatterns = [
     url(r'^admin/', admin.site.urls),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#Auth
urlpatterns += [
    url(r'^api/v1/auth', include('rest_framework.urls',namespace="rest_framework")),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^api/v1/registration/', include('rest_auth.registration.urls')),
    url(r'^api/v1/login',CustomAuthToken.as_view()),
    url(r'^api/v1/logout',CustomAuthLogout.as_view()),
    url(r'^api/v1/', include('models.student.urls')),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#models
urlpatterns += [
    url(r'^api/v1/', include('models.career.urls')),
    url(r'^api/v1/', include('models.university.urls')),
    url(r'^api/v1/', include('models.faculty.urls')),
    url(r'^api/v1/', include('models.building.urls')),
    url(r'^api/v1/', include('models.classes.urls')),
    url(r'^api/v1/', include('models.classroom.urls')),
    url(r'^api/v1/', include('models.group.urls')),
    url(r'^api/v1/', include('models.group_student.urls')),
    url(r'^api/v1/', include('models.reference.urls')),
    url(r'^api/v1/', include('models.subject.urls')),
    url(r'^api/v1/', include('models.subject_career.urls')),
    url(r'^api/v1/', include('models.subject_student_note.urls')),
    url(r'^api/v1/', include('models.subject_taken_student.urls')),
    url(r'^api/v1/', include('models.subject_taken_student_note.urls')),
    url(r'^api/v1/', include('models.teacher.urls')),
    url(r'^api/v1/', include('models.typology.urls')),
    url(r'^api/v1/', include('models.typology_university.urls')),
    url(r'^api/v1/', include('models.history.urls')),
    url(r'^', home,name="home"),
    url(r'^docs', include('django.contrib.auth.urls')),
    ]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += router.urls
