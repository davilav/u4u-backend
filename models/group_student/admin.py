# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models.group_student.models import GroupByStudent

admin.site.register(GroupByStudent)
