from django.apps import AppConfig


class GroupStudentConfig(AppConfig):
    name = 'group_student'
