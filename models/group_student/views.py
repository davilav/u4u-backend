from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import generics
from .models import GroupByStudent
from .serializers import GroupByStudentSerializer
from django.shortcuts import get_object_or_404

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny

@permission_classes([AllowAny])
class GroupByStudentList(generics.ListCreateAPIView):
    queryset = GroupByStudent.objects.all()
    serializer_class = GroupByStudentSerializer

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(
            queryset,
            pk=self.kwargs['pk'],
        )
        return obj
@permission_classes([AllowAny])
class GroupByStudentDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = GroupByStudent.objects.all()
    serializer_class = GroupByStudentSerializer
