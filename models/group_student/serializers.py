from .models import GroupByStudent
from rest_framework import serializers

class GroupByStudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = GroupByStudent
        fields = ('id','student','group','status')
