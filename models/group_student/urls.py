from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.group_student.views import GroupByStudentList, GroupByStudentDetail

urlpatterns = [
    url(r'^groupstudent/$', GroupByStudentList.as_view(), name='groups'),
    url(r'^groupstudent/(?P<pk>[0-9]+)/$', GroupByStudentDetail.as_view(), name='groups'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
