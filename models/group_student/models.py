from django.db import models
from models.student.models import Student
from models.group.models import Group

class GroupByStudent(models.Model):
    student = models.ForeignKey(Student, null=True, blank=True, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, null=True, blank=True, on_delete=models.CASCADE)
    status = models.IntegerField()

    def __str__(self):
        return self.status

    class Meta:
        ordering = ('id',)
