from django.apps import AppConfig


class SubjectTakenStudentConfig(AppConfig):
    name = 'subject_taken_student'
