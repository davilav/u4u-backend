# Generated by Django 2.2.4 on 2020-04-12 19:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('student', '0004_auto_20200412_1446'),
        ('group_student', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentBySubjectTaken',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(default=None, max_length=150)),
                ('year', models.IntegerField()),
                ('semester', models.IntegerField()),
                ('group_student', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='group_student.GroupByStudent')),
                ('student', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='student.Student')),
            ],
            options={
                'ordering': ('id',),
            },
        ),
    ]
