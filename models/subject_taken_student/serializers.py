from .models import StudentBySubjectTaken
from rest_framework import serializers

class StudentBySubjectTakenSerializer(serializers.ModelSerializer):

    class Meta:
        model = StudentBySubjectTaken
        fields = ('id','student','group_student','description','year','semester')
