from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.subject_taken_student.views import StudentBySubjectTakenList, StudentBySubjectTakenDetail

urlpatterns = [
    url(r'^studentbysubjecttaken/$', StudentBySubjectTakenList.as_view(), name='studentbysubjecttaken'),
    url(r'^studentbysubjecttaken/(?P<pk>[0-9]+)/$', StudentBySubjectTakenDetail.as_view(), name='studentbysubjecttaken'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
