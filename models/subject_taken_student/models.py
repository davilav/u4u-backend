from django.db import models
from models.student.models import Student
from models.group_student.models import GroupByStudent

class StudentBySubjectTaken(models.Model):
    student = models.ForeignKey(Student, null=True, blank=True, on_delete=models.CASCADE)
    group_student = models.ForeignKey(GroupByStudent, null=True, blank=True, on_delete=models.CASCADE)
    description  = models.CharField(max_length=150, default = None)
    year = models.IntegerField()
    semester = models.IntegerField()


    def __str__(self):
        return self.semester

    class Meta:
        ordering = ('id',)
