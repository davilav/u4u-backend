from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import generics
from .models import StudentBySubjectTaken
from .serializers import StudentBySubjectTakenSerializer
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny

@permission_classes([AllowAny])
class StudentBySubjectTakenList(generics.ListCreateAPIView):
    queryset = StudentBySubjectTaken.objects.all()
    serializer_class = StudentBySubjectTakenSerializer

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(
            queryset,
            pk=self.kwargs['pk'],
        )
        return obj

@permission_classes([AllowAny])
class StudentBySubjectTakenDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = StudentBySubjectTaken.objects.all()
    serializer_class = StudentBySubjectTakenSerializer
