# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models.building.models import Building

admin.site.register(Building)
