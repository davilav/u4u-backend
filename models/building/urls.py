from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.building.views import BuildingList, BuildingDetail

urlpatterns = [
    url(r'^builds/$', BuildingList.as_view(), name='builds'),
    url(r'^builds/(?P<pk>[0-9]+)/$', BuildingDetail.as_view(), name='builds'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
