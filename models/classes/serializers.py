from .models import Class
from rest_framework import serializers

class ClassSerializer(serializers.ModelSerializer):

    class Meta:
        model = Class
        fields = ('id','day','time_start', 'time_end', 'group')
