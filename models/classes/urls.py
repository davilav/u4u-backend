from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.classes.views import ClassList, ClassDetail

urlpatterns = [
    url(r'^classes/$', ClassList.as_view(), name='classes'),
    url(r'^classes/(?P<pk>[0-9]+)/$', ClassDetail.as_view(), name='classes'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
