from django.db import models
from models.group.models import Group

class Class(models.Model):
    day = models.CharField(max_length=50, default = None)
    time_start = models.CharField(max_length=50, default = None)
    time_end = models.CharField(max_length=50, default = None)
    group = models.ForeignKey(Group, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.day

    class Meta:
        ordering = ('id',)
