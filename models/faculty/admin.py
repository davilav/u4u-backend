# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models.faculty.models import Faculty

admin.site.register(Faculty)
