from django.db import models
from models.university.models import University

class Faculty(models.Model):
    name = models.CharField(max_length=50, default = None)
    university = models.ForeignKey(University, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)
