from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import generics
from .models import Faculty
from .serializers import FacultySerializer
from django.shortcuts import get_object_or_404

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny

@permission_classes([AllowAny])
class FacultyListByUniversity(generics.ListCreateAPIView):
    queryset = Faculty.objects.all()
    serializer_class = FacultySerializer

    def get_queryset(self):
        university_id = self.kwargs['university_id']
        if 'faculty_id' in self.kwargs:
            faculty_id = self.kwargs['faculty_id']
            return Faculty.objects.filter(university=university_id,pk=faculty_id).distinct()
        else:
            return Faculty.objects.filter(university=university_id).distinct()

class FacultyDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Faculty.objects.all()
    serializer_class = FacultySerializer


@permission_classes([AllowAny])
class FacultyList(generics.ListCreateAPIView):
    queryset = Faculty.objects.all()
    serializer_class = FacultySerializer
