from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.faculty.views import FacultyList, FacultyDetail, FacultyListByUniversity

urlpatterns = [
    url(r'^faculties/$', FacultyList.as_view(), name='faculty'),
    url(r'^faculty/(?P<pk>[0-9]+)/$', FacultyDetail.as_view(), name='faculty'),
    url(r'^university/(?P<university_id>.+)/faculties/$', FacultyListByUniversity.as_view(), name='facultiesbyuniversity'),
    url(r'^university/(?P<university_id>.+)/faculty(?P<faculty_id>.+)/$', FacultyListByUniversity.as_view(), name='facultiesbyuniversity'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
