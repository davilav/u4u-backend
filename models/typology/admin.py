# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models.typology.models import Typology

admin.site.register(Typology)
