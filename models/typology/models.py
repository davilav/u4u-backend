from django.db import models

class Typology(models.Model):
    name = models.CharField(max_length=350, default = None)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('id',)
