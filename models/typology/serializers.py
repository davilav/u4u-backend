from .models import Typology
from rest_framework import serializers

class TypologySerializer(serializers.ModelSerializer):

    class Meta:
        model = Typology
        fields = ('id','name')
