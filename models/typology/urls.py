from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.typology.views import TypologyList, TypologyDetail

urlpatterns = [
    url(r'^typologies/$', TypologyList.as_view(), name='typologies'),
    url(r'^typologies/(?P<pk>[0-9]+)/$', TypologyDetail.as_view(), name='typologies'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
