from django.apps import AppConfig


class TypologyConfig(AppConfig):
    name = 'typology'
