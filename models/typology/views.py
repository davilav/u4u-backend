from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import generics
from .models import Typology
from .serializers import TypologySerializer
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny

@permission_classes([AllowAny])
class TypologyList(generics.ListCreateAPIView):
    queryset = Typology.objects.all()
    serializer_class = TypologySerializer

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(
            queryset,
            pk=self.kwargs['pk'],
        )
        return obj

@permission_classes([AllowAny])
class TypologyDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Typology.objects.all()
    serializer_class = TypologySerializer
