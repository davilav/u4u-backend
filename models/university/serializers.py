from .models import University
from rest_framework import serializers

class UniversitySerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = University
        fields = ('id','name','code', 'credits_number')
