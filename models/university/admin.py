# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models.university.models import University

admin.site.register(University)
