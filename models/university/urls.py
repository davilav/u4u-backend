from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.university.views import UniversityList, UniversityDetail

urlpatterns = [
    url(r'^university/$', UniversityList.as_view(), name='university'),
    url(r'^university/(?P<pk>[0-9]+)/$', UniversityDetail.as_view(), name='university'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
