from django.db import models

class University(models.Model):
    name = models.CharField(max_length=90)
    code = models.CharField(max_length=10)
    credits_number = models.IntegerField(default = None)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)
