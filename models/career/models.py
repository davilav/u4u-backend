from django.db import models
from models.faculty.models import Faculty

class Career(models.Model):
    name = models.CharField(max_length=50, default = None)
    credits_number = models.IntegerField()
    faculty = models.ForeignKey(Faculty, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('id',)
