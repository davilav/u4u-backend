# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models.career.models import Career

admin.site.register(Career)
