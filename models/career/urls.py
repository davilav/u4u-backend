from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.career.views import CareerList, CareerDetail, CareerListByFaculty

urlpatterns = [
    url(r'^careers/$', CareerList.as_view(), name='careers'),
    url(r'^careers/(?P<pk>[0-9]+)/$', CareerDetail.as_view(), name='careers'),
    url(r'^faculty/(?P<faculty_id>.+)/careers/$', CareerListByFaculty.as_view(), name='careersbyfaculty'),
    url(r'^faculty/(?P<faculty_id>.+)/career/(?P<career_id>.+)/$', CareerListByFaculty.as_view(), name='careersbyfaculty'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
