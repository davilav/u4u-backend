from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import generics
from .models import Career
from .serializers import CareerSerializer
from django.shortcuts import get_object_or_404

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny

@permission_classes([AllowAny])
class CareerListByFaculty(generics.ListCreateAPIView):
    queryset = Career.objects.all()
    serializer_class = CareerSerializer

    def get_queryset(self):
        faculty_id = self.kwargs['faculty_id']
        if 'career_id' in self.kwargs:
            career_id = self.kwargs['career_id']
            return Career.objects.filter(faculty=faculty_id,pk=career_id).distinct()
        else:
            return Career.objects.filter(faculty=faculty_id).distinct()

@permission_classes([AllowAny])
class CareerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Career.objects.all()
    serializer_class = CareerSerializer

@permission_classes([AllowAny])
class CareerList(generics.ListCreateAPIView):
    queryset = Career.objects.all()
    serializer_class = CareerSerializer
 
