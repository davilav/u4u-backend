from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.reference.views import ReferenceList, ReferenceDetail

urlpatterns = [
    url(r'^references/$', ReferenceList.as_view(), name='references'),
    url(r'^references/(?P<pk>[0-9]+)/$', ReferenceDetail.as_view(), name='references'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
