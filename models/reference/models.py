from django.db import models
from models.student.models import Student
from models.teacher.models import Teacher
from models.subject.models import Subject

class Reference(models.Model):
    student = models.ForeignKey(Student, null=True, blank=True, on_delete=models.CASCADE)
    teacher = models.ForeignKey(Teacher, null=True, blank=True, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, null=True, blank=True, on_delete=models.CASCADE)
    classes_qualification = models.FloatField(default = None)
    experience_qualification = models.FloatField(default = None)
    student_qualification = models.FloatField(default = None)
    explication_qualification = models.FloatField(default = None)
    amenity_qualification = models.FloatField(default = None)
    final_note = models.FloatField(default = None)
    description = models.CharField(max_length=350, default = None)
    year = models.IntegerField()
    semester = models.CharField(max_length=350, default = None)

    def __str__(self):
        return self.qualification

    class Meta:
        ordering = ('id',)
