from .models import Reference
from rest_framework import serializers

class ReferenceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reference
        fields = (
        'id',
        'teacher',
        'student',
        'subject',
        'final_note',
        'classes_qualification',
        'experience_qualification',
        'student_qualification',
        'explication_qualification',
        'amenity_qualification',
        'description',
        'year',
        'semester')
