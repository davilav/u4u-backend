from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import generics
from .models import Teacher, AverageByTeacherView, ReferenceByTeacherView, SubjectsByTeacherView
from .serializers import TeacherSerializer, AverageByTeacherViewSerializer, ReferenceByTeacherViewSerializer, SubjectsByTeacherViewSerializer
from django.shortcuts import get_object_or_404
from django.db.models import Q
from .pagination import ReferenceLimitOffsetPagination, ReferencePageNumberPagination
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny

@permission_classes([AllowAny])
class TeacherList(generics.ListCreateAPIView):
    serializer_class = TeacherSerializer

    def get_queryset(self, *args, **kwargs):
        queryset_list = Teacher.objects.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                Q(name__icontains=query)
            ).distinct()
        return queryset_list

@permission_classes([AllowAny])
class TeacherDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer


@permission_classes([AllowAny])
class AverageByTeacherViewList(generics.ListAPIView):
    serializer_class = AverageByTeacherViewSerializer

    def get_queryset(self):
        teacher_id = self.kwargs['teacher_id']
        return AverageByTeacherView.objects.filter(id=teacher_id).distinct()


@permission_classes([AllowAny])
class ReferenceByTeacherViewList(generics.ListCreateAPIView):
    serializer_class = ReferenceByTeacherViewSerializer
    pagination_class = ReferencePageNumberPagination

    def get_queryset(self):
        teacher_id = self.kwargs['teacher_id']
        return ReferenceByTeacherView.objects.filter(id=teacher_id).distinct()


@permission_classes([AllowAny])
class SubjectsByTeacherViewList(generics.ListCreateAPIView):
    serializer_class = SubjectsByTeacherViewSerializer

    def get_queryset(self):
        teacher_id = self.kwargs['teacher_id']
        return SubjectsByTeacherView.objects.filter(id=teacher_id).distinct()
