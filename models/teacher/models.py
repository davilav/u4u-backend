from django.db import models

class Teacher(models.Model):
    name = models.CharField(max_length=250, default = None, unique = True)
    status = models.IntegerField(default = None, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('id',)


class AverageByTeacherView(models.Model):
    name = models.CharField(max_length=150, default = None)
    classes_average = models.FloatField(default = None)
    experience_average = models.FloatField(default = None)
    student_average = models.FloatField(default = None)
    explication_average = models.FloatField(default = None)
    amenity_average = models.FloatField(default = None)
    note_average = models.FloatField(default = None)

    def __str__(self):
        return '%s: %s' % (self.name, self.note_average)

    class Meta:
        managed = False
        db_table = 'vw_averages_by_teacher'
        ordering = ('id',)


class ReferenceByTeacherView(models.Model):
    name = models.CharField(max_length=350, default = None)
    classes_qualification = models.FloatField(default = None)
    experience_qualification = models.FloatField(default = None)
    student_qualification = models.FloatField(default = None)
    explication_qualification = models.FloatField(default = None)
    amenity_qualification = models.FloatField(default = None)
    final_note = models.FloatField(default = None)
    description = models.CharField(max_length=350, default = None)
    year = models.IntegerField()
    semester = models.CharField(max_length=350, default = None)

    def __str__(self):
        return '%s: %s' % (self.name, self.note_qualification)

    class Meta:
        managed = False
        db_table = 'vw_references_by_teacher'
        ordering = ('id',)

class SubjectsByTeacherView(models.Model):
    name = models.CharField(max_length=350, default = None)
    subject_id = models.IntegerField(default = None)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'vw_subjects_by_teacher'
        ordering = ('id',)
