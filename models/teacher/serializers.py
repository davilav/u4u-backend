from .models import Teacher, AverageByTeacherView, ReferenceByTeacherView, SubjectsByTeacherView
from rest_framework import serializers

class TeacherSerializer(serializers.ModelSerializer):

    class Meta:
        model = Teacher
        fields = ('id','name','status')

class AverageByTeacherViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = AverageByTeacherView
        fields = (
        'id',
        'name',
        'note_average',
        'classes_average',
        'experience_average',
        'student_average',
        'explication_average',
        'amenity_average',
        )

class ReferenceByTeacherViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReferenceByTeacherView
        fields = (
        'id',
        'name',
        'final_note',
        'classes_qualification',
        'experience_qualification',
        'student_qualification',
        'explication_qualification',
        'amenity_qualification',
        'description',
        'year',
        'semester')

class SubjectsByTeacherViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubjectsByTeacherView
        fields = ('id','subject_id','name')
