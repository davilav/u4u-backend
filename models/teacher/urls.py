from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.teacher.views import TeacherList, TeacherDetail, AverageByTeacherViewList, ReferenceByTeacherViewList, SubjectsByTeacherViewList

urlpatterns = [
    url(r'^teachers/$', TeacherList.as_view(), name='teachers'),
    url(r'^teachers/(?P<pk>[0-9]+)/$', TeacherDetail.as_view(), name='teachers'),
    url(r'teacher/(?P<teacher_id>.+)/averages/$', AverageByTeacherViewList.as_view(), name='views.averagesbyteacher'),
    url(r'teacher/(?P<teacher_id>.+)/subjects/$', SubjectsByTeacherViewList.as_view(), name='views.subjectsbyteacher'),
    url(r'^teacher/(?P<teacher_id>.+)/references/$', ReferenceByTeacherViewList.as_view(), name='referencesbyteacher'),
    url(r'^teacher/(?P<teacher_id>.+)/career/(?P<references_id>.+)/$', ReferenceByTeacherViewList.as_view(), name='referencesbyteachers'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
