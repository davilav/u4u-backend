# Generated by Django 2.2.4 on 2020-05-17 10:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teacher', '0005_referencebyteacherview_subjectsbyteacherview'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='status',
            field=models.IntegerField(blank=True, default=None),
        ),
    ]
