from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.group.views import GroupList, GroupDetail

urlpatterns = [
    url(r'^groups/$', GroupList.as_view(), name='groups'),
    url(r'^groups/(?P<pk>[0-9]+)/$', GroupDetail.as_view(), name='groups'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
