from django.db import models
from models.teacher.models import Teacher
from models.subject_career.models import SubjectByCareer
from models.classroom.models import Classroom

class Group(models.Model):
    number = models.CharField(max_length=20, default = None)
    subject = models.ForeignKey(SubjectByCareer, null=True, blank=True, on_delete=models.CASCADE)
    teacher = models.ForeignKey(Teacher, null=True, blank=True, on_delete=models.CASCADE)
    classroom = models.ForeignKey(Classroom, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.number

    class Meta:
        ordering = ('id',)
