from django.db import models
from models.building.models import Building

class Classroom(models.Model):
    name = models.CharField(max_length=50, default = None)
    building = models.ForeignKey(Building, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('id',)
