from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.classroom.views import ClassroomList, ClassroomDetail

urlpatterns = [
    url(r'^classroom/$', ClassroomList.as_view(), name='classroom'),
    url(r'^classroom/(?P<pk>[0-9]+)/$', ClassroomDetail.as_view(), name='classroom'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
