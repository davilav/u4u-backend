# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models.classroom.models import Classroom

admin.site.register(Classroom)
