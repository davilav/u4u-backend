from django.apps import AppConfig


class SubjectPrerequisitesConfig(AppConfig):
    name = 'subject_prerequisites'
