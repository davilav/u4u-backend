from django.db import models
from models.university.models import University
from models.typology.models import Typology

class TypologyByUniversity(models.Model):
    university = models.ForeignKey(University, null=True, blank=True, on_delete=models.CASCADE)
    typology = models.ForeignKey(Typology, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        ordering = ('id',)
