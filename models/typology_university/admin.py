# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models.typology_university.models import TypologyByUniversity

admin.site.register(TypologyByUniversity)
