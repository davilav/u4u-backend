from django.apps import AppConfig


class TypologyUniversityConfig(AppConfig):
    name = 'typology_university'
