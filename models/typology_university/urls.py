from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.typology_university.views import TypologyByUniversityList, TypologyByUniversityDetail

urlpatterns = [
    url(r'^typologybyuniversity/$', TypologyByUniversityList.as_view(), name='typologybyuniversity'),
    url(r'^typologybyuniversity/(?P<pk>[0-9]+)/$', TypologyByUniversityDetail.as_view(), name='typologybyuniversity'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
