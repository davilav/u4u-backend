from .models import TypologyByUniversity
from rest_framework import serializers

class TypologyByUniversitySerializer(serializers.ModelSerializer):

    class Meta:
        model = TypologyByUniversity
        fields = ('id','university','typology')
