from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import generics
from .models import NoteStudentBySubject
from .serializers import NoteStudentBySubjectSerializer
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny

@permission_classes([AllowAny])
class NoteByStudentAndBySubjectList(generics.ListCreateAPIView):
    queryset = NoteStudentBySubject.objects.all()
    serializer_class = NoteStudentBySubjectSerializer

    def get_queryset(self):
        student_id = self.kwargs['student_id']
        if 'group_id' in self.kwargs:
            group_id = self.kwargs['group_id']
            return NoteStudentBySubject.objects.filter(group=group_id,student=student_id).distinct()
        else:
            return NoteStudentBySubject.objects.filter(student=student_id).distinct()

@permission_classes([AllowAny])
class NoteStudentBySubjectDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = NoteStudentBySubject.objects.all()
    serializer_class = NoteStudentBySubjectSerializer


@permission_classes([AllowAny])
class NoteStudentBySubjectList(generics.ListCreateAPIView):
    queryset = NoteStudentBySubject.objects.all()
    serializer_class = NoteStudentBySubjectSerializer
