from django.db import models
from models.group.models import Group
from models.student.models import Student

class NoteStudentBySubject(models.Model):
    group = models.ForeignKey(Group, null=True, blank=True, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, null=True, blank=True, on_delete=models.CASCADE)
    value = models.FloatField()
    percent = models.FloatField()
    description  = models.CharField(max_length=150, default = None)

    def __str__(self):
        return self.description

    class Meta:
        ordering = ('id',)
