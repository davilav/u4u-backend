from django.apps import AppConfig


class SubjectStudentNoteConfig(AppConfig):
    name = 'subject_student_note'
