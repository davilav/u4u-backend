# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models.subject_student_note.models import NoteStudentBySubject

admin.site.register(NoteStudentBySubject)
