from .models import NoteStudentBySubject
from rest_framework import serializers

class NoteStudentBySubjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = NoteStudentBySubject
        fields = ('id','student','group','value','percent','description')
