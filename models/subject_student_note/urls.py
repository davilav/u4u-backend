from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.subject_student_note.views import NoteStudentBySubjectList, NoteStudentBySubjectDetail, NoteByStudentAndBySubjectList

urlpatterns = [
    url(r'^notestudentbysubject/$', NoteStudentBySubjectList.as_view(), name='notestudentbysubject'),
    url(r'^notestudentbysubject/(?P<pk>[0-9]+)/$', NoteStudentBySubjectDetail.as_view(), name='notestudentbysubject'),
    url(r'^student/(?P<student_id>.+)/group/notes/(?P<group_id>.+)/$',  NoteByStudentAndBySubjectList.as_view(), name='notebystudentbysubject')
]

urlpatterns = format_suffix_patterns(urlpatterns)
