from .models import StudentBySubjectTakenNote
from rest_framework import serializers

class StudentBySubjectTakenNoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = StudentBySubjectTakenNote
        fields = ('id','subject_taken','value','percent','description')
