# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models.subject_taken_student_note.models import StudentBySubjectTakenNote

admin.site.register(StudentBySubjectTakenNote)
