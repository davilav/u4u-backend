from django.apps import AppConfig


class SubjectTakenStudentNoteConfig(AppConfig):
    name = 'subject_taken_student_note'
