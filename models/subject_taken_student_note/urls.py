from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.subject_taken_student_note.views import StudentBySubjectTakenNoteList, StudentBySubjectTakenNoteDetail

urlpatterns = [
    url(r'^studentbysubjecttakennote/$', StudentBySubjectTakenNoteList.as_view(), name='studentbysubjecttakennote'),
    url(r'^studentbysubjecttakennote/(?P<pk>[0-9]+)/$', StudentBySubjectTakenNoteDetail.as_view(), name='studentbysubjecttakennote'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
