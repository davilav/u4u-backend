from django.db import models
from models.subject_taken_student.models import StudentBySubjectTaken

class StudentBySubjectTakenNote(models.Model):
    subject_taken = models.ForeignKey(StudentBySubjectTaken, null=True, blank=True, on_delete=models.CASCADE)
    value = models.FloatField()
    percent = models.FloatField()
    description  = models.CharField(max_length=150, default = None)

    def __str__(self):
        return self.value

    class Meta:
        ordering = ('id',)
