# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from models.career.models import Career
from models.university.models import University
from models.faculty.models import Faculty

class Student(models.Model):
    user = models.OneToOneField(User,primary_key = True, on_delete = models.CASCADE)
    career = models.ForeignKey(Career, null=True, default=None, on_delete=models.CASCADE)
    university = models.ForeignKey(University, null=True, default=None, on_delete=models.CASCADE)
    faculty = models.ForeignKey(Faculty, null=True, default=None, on_delete=models.CASCADE)
    gender = models.CharField(max_length = 20, default=None)
    credits_semester = models.IntegerField(default = 0,blank=True, null=False)
    birth_date = models.CharField(max_length = 12, default=None)
    new_student = models.IntegerField(default = 0,blank=True, null=False)
    first_login = models.IntegerField(default = 1,blank=True, null=False)

    def __str__(self):
        return 'User: {}'.format(self.user)

    class Meta:
        ordering = ('university',)

class GroupAndClassByStudentView(models.Model):
    subject_id = models.IntegerField()
    group_id = models.IntegerField()
    classes_id = models.IntegerField()
    group_student = models.IntegerField()
    group_number = models.CharField(max_length=50, default = None)
    day = models.CharField(max_length=50, default = None)
    time_start = models.CharField(max_length=50, default = None)
    time_end = models.CharField(max_length=50, default = None)
    teacher_name = models.CharField(max_length=350, default = None)
    subject_name = models.CharField(max_length=350, default = None)
    credits_number = models.IntegerField()
    classroom_name = models.CharField(max_length=350, default = None)
    building_name = models.CharField(max_length=350, default = None)


    def __str__(self):
        return '%s: %s' % (self.subject_name, self.teacher_name)

    class Meta:
        managed = False
        db_table = 'vw_classes_and_groups_by_student'
        ordering = ('subject_id',)


class HistoryByStudentView(models.Model):
    credits_semester = models.IntegerField(default = 0)
    average = models.FloatField(default = 0)
    credits_number = models.IntegerField(default = 0)
    completed_credits = models.IntegerField(default = 0)
    progress = models.FloatField(default = 0)


    def __str__(self):
        return '%s: %s' % (self.completed_credits, self.progress)

    class Meta:
        managed = False
        db_table = 'vw_history_by_student'
        ordering = ('id',)


class SubjectsByStudentView(models.Model):
    subject = models.CharField(max_length=350, default = None)
    group = models.IntegerField()
    credits_number = models.IntegerField()
    note_acumulated = models.FloatField()
    positive_note = models.FloatField()
    percent_acumulated = models.IntegerField()


    def __str__(self):
        return '%s: %s' % (self.subject, self.positive_note)

    class Meta:
        managed = False
        db_table = 'vw_subjects_by_student'
        ordering = ('id',)
