# Generated by Django 2.2.4 on 2020-05-17 10:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0012_student_first_login'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoryByStudentView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('credits_semester', models.IntegerField(default=0)),
                ('average', models.FloatField(default=0)),
                ('credits_number', models.IntegerField(default=0)),
                ('completed_credits', models.IntegerField(default=0)),
                ('progress', models.FloatField(default=0)),
            ],
            options={
                'db_table': 'vw_history_by_student',
                'ordering': ('id',),
                'managed': False,
            },
        ),
        migrations.RemoveField(
            model_name='student',
            name='photo',
        ),
        migrations.AddField(
            model_name='student',
            name='new_student',
            field=models.IntegerField(default=0),
        ),
    ]
