from rest_framework import serializers
from .models import Student,Career,University,Faculty, GroupAndClassByStudentView, HistoryByStudentView, SubjectsByStudentView
from models.career.serializers import CareerSerializer
from models.university.serializers import UniversitySerializer
from models.faculty.serializers import FacultySerializer
from django.contrib.auth.models import User
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from .tokens import account_activation_token
from rest_framework import status
from django.conf import settings
import datetime
from django.contrib.auth.signals import user_logged_in

class CurrentUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username', 'email','first_name','last_name','is_staff')

    def create(self, validated_data):
        password = validated_data['password1']
        if validated_data['password1'] == validated_data['password2']:
            del validated_data["password1"]
            del validated_data["password2"]
        user = super(CurrentUserSerializer, self).create(validated_data)
        user.set_password(password)
        user.save()
        return user

class StudentSerializer(serializers.ModelSerializer):
    user = CurrentUserSerializer(many=False,read_only=True)

    class Meta:
        model = Student
        fields = ('user','credits_semester','first_login','new_student','gender','birth_date','career','university','faculty')

    def create(self, validated_data):
        career = Career.objects.get(pk=validated_data.pop('career'))
        university = University.objects.get(pk=validated_data.pop('university'))
        faculty = Faculty.objects.get(pk=validated_data.pop('faculty'))
        credits_semester = validated_data.pop('credits_semester')
        first_login = validated_data.pop('first_login')
        new_student = validated_data.pop('new_student')
        gender = validated_data.pop('gender')
        birth_date = validated_data.pop('birth_date')
        user_data = validated_data.pop('user')
        user = CurrentUserSerializer.create(CurrentUserSerializer(), validated_data=user_data)
        student, created = Student.objects.update_or_create(user=user,career=career,faculty=faculty,university=university,credits_semester=credits_semester,first_login=first_login,new_student=new_student,gender=gender,birth_date=birth_date)
        return student


class CustomAuthToken(ObtainAuthToken):
    def post(self, request):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        if serializer.is_valid():
            token, created = Token.objects.get_or_create(user=serializer.validated_data['user'])
            user = serializer.validated_data['user']
            user_logged_in.send(sender=user.__class__, request=request, user=user)

            token.delete()
            token = Token.objects.create(user=user)
            token.save()

            student = Student.objects.get(pk=user.pk)

            return Response({
                'token': token.key,
                'user_id': user.pk,
                'last_login': user.last_login,
                'username': user.username,
                'email': user.email,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'first_login': student.first_login,
                'new_student': student.new_student,
                'credits_semester': student.credits_semester,
                'gender': student.gender,
                'birth_date': student.birth_date,
                'career': student.career_id,
                'university': student.university_id,
                'faculty': student.faculty_id,
            })
        return Response({"Error":121}, status=status.HTTP_400_BAD_REQUEST)


class CustomAuthLogout(ObtainAuthToken):

    def post(self, request):
        return self.logout(request)

    def logout(self, request):
        request.user.auth_token.delete()
        return Response({"Success": "Successfully logged out."}, status=status.HTTP_200_OK)

obtain_expiring_auth_token = CustomAuthToken.as_view()


class GroupAndClassByStudentViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupAndClassByStudentView
        fields = (
        'id',
        'group_id',
        'classes_id',
        'group_student',
        'group_number',
        'day',
        'time_start',
        'time_end',
        'teacher_name',
        'subject_name',
        'credits_number',
        'building_name',
        'classroom_name',
        )

class HistoryByStudentViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = HistoryByStudentView
        fields = (
        'id',
        'credits_number',
        'credits_semester',
        'completed_credits',
        'average',
        'progress',
        )

class SubjectsByStudentViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubjectsByStudentView
        fields = (
        'id',
        'group',
        'credits_number',
        'subject',
        'note_acumulated',
        'positive_note',
        'percent_acumulated',
        )
