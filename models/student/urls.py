from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from django.contrib.auth import views as auth_views
from models.student.views import StudentView, StudentDetail, GroupAndClassByStudentViewList, HistoryByStudentViewList, SubjectsByStudentViewList
from . import views

urlpatterns = [
    url(r'^students/$', StudentView.as_view(), name='students'),
    url(r'^students/(?P<pk>[0-9]+)/$', StudentDetail.as_view(), name='students'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate, name='activate'),
    url(r'student/(?P<student_id>.+)/subjects/$', SubjectsByStudentViewList.as_view(), name='views.subjectsbystudent'),
    url(r'student/(?P<student_id>.+)/history/$', HistoryByStudentViewList.as_view(), name='views.historybystudent'),
    url(r'student/(?P<student_id>.+)/groups/$', GroupAndClassByStudentViewList.as_view(), name='views.subjectbycareerbyfacultyanduniversity'),
    url(r'student/(?P<student_id>.+)/groups/(?P<group_id>.+)/$', GroupAndClassByStudentViewList.as_view(), name='views.subjectbycareerbyfacultyanduniversity'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
