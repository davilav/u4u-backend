# Generated by Django 2.2.4 on 2020-05-12 01:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('student', '0012_student_first_login'),
    ]

    operations = [
        migrations.CreateModel(
            name='History',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('progress', models.FloatField(default=0)),
                ('average', models.FloatField(default=0)),
                ('completed_credits', models.IntegerField(default=0)),
                ('student', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='student.Student')),
            ],
            options={
                'ordering': ('id',),
            },
        ),
    ]
