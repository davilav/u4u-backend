from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.history.views import HistoryList, HistoryDetail

urlpatterns = [
    url(r'^history/$', HistoryList.as_view(), name='history'),
    url(r'^history/(?P<pk>[0-9]+)/$', HistoryDetail.as_view(), name='history'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
