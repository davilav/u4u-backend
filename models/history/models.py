from django.db import models
from models.student.models import Student

class History(models.Model):
    student = models.ForeignKey(Student, null=True, blank=True, on_delete=models.CASCADE)
    average = models.FloatField(default = 0)
    completed_credits = models.IntegerField(default = 0)

    def __str__(self):
        return self.student

    class Meta:
        ordering = ('id',)
