from django.apps import AppConfig


class SubjectCareerConfig(AppConfig):
    name = 'subject_career'
