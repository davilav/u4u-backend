from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.subject_career.views import SubjectByCareerList, SubjectByCareerDetail

urlpatterns = [
    url(r'^subjectsbycareer/$', SubjectByCareerList.as_view(), name='subjectsbycareer'),
    url(r'^subjectsbycareer/(?P<pk>[0-9]+)/$', SubjectByCareerDetail.as_view(), name='subjectsbycareer'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
