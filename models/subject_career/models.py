from django.db import models
from models.subject.models import Subject
from models.career.models import Career

class SubjectByCareer(models.Model):
    subject = models.ForeignKey(Subject, null=True, blank=True, on_delete=models.CASCADE)
    career = models.ForeignKey(Career, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        ordering = ('id',)
