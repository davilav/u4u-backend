from .models import SubjectByCareer
from rest_framework import serializers

class SubjectByCareerSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubjectByCareer
        fields = ('id','subject','career')
