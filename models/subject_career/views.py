from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import generics
from .models import SubjectByCareer
from .serializers import SubjectByCareerSerializer
from django.shortcuts import get_object_or_404

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny

@permission_classes([AllowAny])
class SubjectByCareerList(generics.ListCreateAPIView):
    queryset = SubjectByCareer.objects.all()
    serializer_class = SubjectByCareerSerializer

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(
            queryset,
            pk=self.kwargs['pk'],
        )
        return obj

@permission_classes([AllowAny])
class SubjectByCareerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = SubjectByCareer.objects.all()
    serializer_class = SubjectByCareerSerializer
