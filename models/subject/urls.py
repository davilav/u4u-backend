from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from models.subject.views import SubjectList, SubjectDetail, SubjectByCareerViewList, GroupAndClassBySubjectViewList

urlpatterns = [
    url(r'^subjects/$', SubjectList.as_view(), name='subjects'),
    url(r'^subjects/(?P<pk>[0-9]+)/$', SubjectDetail.as_view(), name='subjects'),
    url(r'career/(?P<career_id>.+)/subjects/$', SubjectByCareerViewList.as_view(), name='views.subjectsbycareerbyfacultyanduniversity'),
    url(r'career/(?P<career_id>.+)/subject/(?P<subject_id>.+)/$', SubjectByCareerViewList.as_view(), name='views.subjectbycareerbyfacultyanduniversity'),
    url(r'subject/(?P<subject_id>.+)/groups/$', GroupAndClassBySubjectViewList.as_view(), name='views.groupandclassbysubject'),
    url(r'subject/(?P<subject_id>.+)/groups/(?P<group_id>.+)/$', GroupAndClassBySubjectViewList.as_view(), name='views.groupandclassbysubject'),
    url(r'subject/(?P<subject_id>.+)/groups/(?P<group_id>.+)/classes/$', GroupAndClassBySubjectViewList.as_view(), name='views.groupandclassbysubject'),
    url(r'subject/(?P<subject_id>.+)/groups/(?P<group_id>.+)/class/(?P<classes_id>.+)/$', GroupAndClassBySubjectViewList.as_view(), name='views.groupandclassbysubject'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
