from .models import Subject, SubjectByCareerView , GroupAndClassBySubjectView
from rest_framework import serializers

class SubjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subject
        fields = ('id','name','credits_number','typology')


class SubjectByCareerViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubjectByCareerView
        fields = (
        'id',
        'career',
        'name',
        'faculty_name',
        'career_name',
        'typology_name',
        )

class GroupAndClassBySubjectViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupAndClassBySubjectView
        fields = (
        'id',
        'subject_id',
        'classes_id',
        'name',
        'day',
        'time_start',
        'time_end',
        'teacher_name',
        'subject_name',
        'credits_number',
        'building_name',
        'classroom_name'
        )
