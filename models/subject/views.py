from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import generics
from .models import Subject, SubjectByCareerView, GroupAndClassBySubjectView
from .serializers import SubjectSerializer, SubjectByCareerViewSerializer, GroupAndClassBySubjectViewSerializer
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny

@permission_classes([AllowAny])
class SubjectList(generics.ListCreateAPIView):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(
            queryset,
            pk=self.kwargs['pk'],
        )
        return obj

@permission_classes([AllowAny])
class SubjectDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer

@permission_classes([AllowAny])
class SubjectByCareerViewList(generics.ListAPIView):
    serializer_class = SubjectByCareerViewSerializer

    def get_queryset(self):
        career_id = self.kwargs['career_id']
        if 'subject_id' in self.kwargs:
            subject_id = self.kwargs['subject_id']
            return SubjectByCareerView.objects.filter(career=career_id,subject=subject_id).distinct()
        else:
            return SubjectByCareerView.objects.filter(career=career_id).distinct()


@permission_classes([AllowAny])
class GroupAndClassBySubjectViewList(generics.ListAPIView):
    serializer_class = GroupAndClassBySubjectViewSerializer

    def get_queryset(self):
        subject_id = self.kwargs['subject_id']
        if 'group_id' in self.kwargs:
            group_id = self.kwargs['group_id']
            return GroupAndClassBySubjectView.objects.filter(subject_id=subject_id,id=group_id).distinct()
        elif 'classes_id' in self.kwargs:
            classes_id = self.kwargs['classes_id']
            return GroupAndClassBySubjectView.objects.filter(subject_id=subject_id,id=group_id,classes_id=classes_id).distinct()
        else:
            return GroupAndClassBySubjectView.objects.filter(subject_id=subject_id).distinct()
