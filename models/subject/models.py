from django.db import models
from models.typology.models import Typology
from models.university.models import University
from models.faculty.models import Faculty
from models.career.models import Career

class Subject(models.Model):
    name = models.CharField(max_length=250, default = None, unique = True)
    credits_number = models.IntegerField()
    typology = models.ForeignKey(Typology, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('id',)

class SubjectByCareerView(models.Model):
    career = models.ForeignKey(Career, null=True, blank=True, on_delete=models.CASCADE, related_name='career_id')
    faculty_name = models.CharField(max_length=150, default = None)
    career_name = models.CharField(max_length=150, default = None)
    name = models.CharField(max_length=350, default = None)
    typology_name = models.CharField(max_length=150, default = None)

    def __str__(self):
        return '%s: %s' % (self.career_name, self.subject_name)

    class Meta:
        managed = False
        db_table = 'vw_subjects_by_career'
        ordering = ('id',)

class GroupAndClassBySubjectView(models.Model):
    subject_id = models.IntegerField()
    classes_id = models.IntegerField()
    typology_name = models.CharField(max_length=150, default = None)
    day = models.CharField(max_length=50, default = None)
    time_start = models.CharField(max_length=50, default = None)
    time_end = models.CharField(max_length=50, default = None)
    name = models.CharField(max_length=50, default = None)
    teacher_name = models.CharField(max_length=350, default = None)
    subject_name = models.CharField(max_length=350, default = None)
    credits_number = models.IntegerField()
    classroom_name = models.CharField(max_length=350, default = None)
    building_name = models.CharField(max_length=350, default = None)

    def __str__(self):
        return '%s: %s' % (self.subject_name, self.teacher_name)

    class Meta:
        managed = False
        db_table = 'vw_subjects_by_classes_by_group'
        ordering = ('classes_id',)
